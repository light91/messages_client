﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;

namespace messages_pcl
{
    public class MessageManager
    {
        private static MessageManager m_instance;
        private SQLiteConnection m_db;
        public List<WeakReference<OnNotifyDataChanged>> m_notify_data_changed;
        private SortedDictionary<String, MessageItem> m_items;

        private MessageManager()
        {
            m_notify_data_changed = new List<WeakReference<OnNotifyDataChanged>>();
            initializeSQLite();
            initializeItems();
        }

        public void initializeSQLite()
        {
            m_db = new SQLiteConnection(ApplicationShared.instance().getISQLitePlatform(), ApplicationShared.instance().getSQLiteDatabaseName());
            var tableExistsQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name='MessageItem';";
            var result = m_db.ExecuteScalar<string>(tableExistsQuery);
            if (result == null)
                m_db.CreateTable<MessageItem>();
        }

        public void initializeItems()
        {
            m_items = new SortedDictionary<String, MessageItem>();

            var table = m_db.Table<MessageItem>();
            var items = table.ToList<MessageItem>();
            foreach (var item in items)
            {
                m_items.Add(item.Id, item);
            }
            System.Diagnostics.Debug.WriteLine("!!!initializeItems m_items: " + m_items.Count);
        }

        public static MessageManager instance()
        {
            if (m_instance == null)
                m_instance = new MessageManager();

            return m_instance;
        }


        public int getCount()
        {
            return m_items.Count;
        }

        public MessageItem get(int position)
        {
            return m_items.Values.ElementAt(position);
        }


        public void add(MessageItem item)
        {
            System.Diagnostics.Debug.WriteLine("add item != null " + (item != null));
            System.Diagnostics.Debug.WriteLine("add item item.Id " + item.Id);
            System.Diagnostics.Debug.WriteLine("add m_items != null " + (m_items != null));
            if (m_items.ContainsKey(item.Id))
                return;
            System.Diagnostics.Debug.WriteLine("add item 1");

            addTableRow(item);
            System.Diagnostics.Debug.WriteLine("add item 2");

            m_items.Add(item.Id, item);

            System.Diagnostics.Debug.WriteLine("add pre");
            notifyDataChanged();
            System.Diagnostics.Debug.WriteLine("add after");
        }

        public void removeAllPrepareItems()
        {
            List<String> collectKeys = new List<String>();
            foreach (var item in m_items)
                if (item.Value.IsRemovedPrepare())
                    collectKeys.Add(item.Key);

            foreach (var key in collectKeys)
            {
                removeFromTableRow(m_items[key]);
                m_items.Remove(key);
            }
        }

        public void addTableRow(MessageItem item)
        {
            System.Diagnostics.Debug.WriteLine("!!!addTableRow pre");
            m_db.Insert(item);
            System.Diagnostics.Debug.WriteLine("!!!addTableRow after");
        }

        public void removeFromTableRow(MessageItem item)
        {
            System.Diagnostics.Debug.WriteLine("!!!removeFromTableRow pre");
            m_db.Delete(item);
            System.Diagnostics.Debug.WriteLine("!!!removeFromTableRow after");
        }

        public void setReaded(MessageItem item)
        {
            m_db.Update(item);
            notifyDataChanged();
        }

        public void AddNotifyDataChangedListener(OnNotifyDataChanged notifyDataChanged)
        {
            m_notify_data_changed.Add(new WeakReference<OnNotifyDataChanged>(notifyDataChanged));
        }

        private void notifyDataChanged()
        {
            foreach (var reference in m_notify_data_changed)
            {
                OnNotifyDataChanged target;
                reference.TryGetTarget(out target);
                target.OnNotifyDataChanged();
            }
        }
    }

    public interface OnNotifyDataChanged
    {
        void OnNotifyDataChanged();
    }
}
