﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using Newtonsoft.Json;


namespace messages_pcl
{
    public class User
    {
        private static String PREFERENCE_NAME_KEY = "PREFERENCE_NAME_KEY";
        private static String PREFERENCE_PASSWORD_KEY = "PREFERENCE_PASSWORD_KEY";

        private String m_phone;
        private String m_password;
        

        public User()
        {
            m_phone = ApplicationShared.instance().getPreference().GetString(PREFERENCE_NAME_KEY);
            m_password = ApplicationShared.instance().getPreference().GetString(PREFERENCE_PASSWORD_KEY);
        }


        public OnLoginReuslt m_login_result { private get; set; }

        
        public void onLoginResult(String phone, String password, bool result)
        {
            if (result)
            {
                m_phone = phone;
                ApplicationShared.instance().getPreference().SetString(PREFERENCE_NAME_KEY, m_phone);

                m_password = password;
                ApplicationShared.instance().getPreference().SetString(PREFERENCE_PASSWORD_KEY, m_password);

            }
            m_login_result.onLoginResult(result);
            //sql.save
        }


        public void onGetPasswordResult(bool result)
        {
            //m_login_result.onLoginResult(result);
            //sql.save
        }


        public String getPhone()
        {
            return m_phone;
        }

        public String getPassword()
        {
            return m_password;
        }
    }

    public interface OnLoginReuslt
    {
        void onLoginResult(bool result);
    }

    public interface OnGetPasswordReuslt
    {
        void onGetPasswordResult(bool result);
    }
}
