﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using System.Runtime.Serialization;

namespace messages_pcl
{
    [DataContract]
    public class PacketGetPassword : Packet
    {
        [DataMember]
        private String m_phone;
        [DataMember]
        private String m_code;
        [DataMember]
        private bool m_result;


        public PacketGetPassword(int typeId) : base(typeId)
        {
        }


        public void setPhone(String phone)
        {
            m_phone = phone;
        }

        public void setCode(String code)
        {
            m_code = code;
        }



        public override void onReceive()
        {
            ApplicationShared.instance().getUser().onGetPasswordResult(m_result);
        }
    }
}
