﻿using System;

using System.IO;



namespace messages_pcl
{
    public class PacketMessage : Packet
    {
        private MessageItem m_message_item;

        public PacketMessage(int typeId) : base(typeId)
        {
        }


        protected override Packet initByJSON(MemoryStream stream)
        {
            System.Diagnostics.Debug.WriteLine("!!!create MessageItem 1");
            m_message_item = MessageItem.initByJSON(stream);
            System.Diagnostics.Debug.WriteLine("!!!create MessageItem 2");
            return this;
        }

        public override void onReceive()
        {
            System.Diagnostics.Debug.WriteLine("!!!create onReceive");
            MessageManager.instance().add(m_message_item);
        }
    }
}
