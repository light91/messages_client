﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Interop;

namespace messages_pcl
{
    public class ApplicationShared
    {
        private static ApplicationShared m_instance;
        private IUserPreferences m_preference;
        private ISQLitePlatform m_sql_platform;
        private String m_path;
        private User m_user;

        private ApplicationShared()
        {
        }

        public static ApplicationShared instance()
        {
            if (m_instance == null)
                m_instance = new ApplicationShared();

            return m_instance;
        }

        public void initialize(IUserPreferences preference, ISQLitePlatform sqlitePlatform, String path)
        {
            m_preference = preference;
            m_sql_platform = sqlitePlatform;
            m_path = path;
            m_user = new User();
        }




        public IUserPreferences getPreference()
        {
            return m_preference;
        }

        public User getUser()
        {
            return m_user;
        }

        public ISQLitePlatform getISQLitePlatform()
        {
            return m_sql_platform;
        }

        public String getSQLiteDatabaseName()
        {
            return m_path;
        }
    }
}
