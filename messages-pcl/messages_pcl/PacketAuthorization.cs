﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace messages_pcl
{
    [DataContract]
    public class PacketAuthorization : Packet
    {
        [DataMember]
        private String m_phone;
        [DataMember]
        private String m_code;
        [DataMember]
        private String m_password;
        [DataMember]
        private bool m_result;


        public PacketAuthorization(int typeId) : base(typeId)
        {
        }


        public void setPhone(String phone)
        {
            m_phone = phone;
        }

        public void setCode(String code)
        {
            m_code = code;
        }

        public void setPassword(String password)
        {
            m_password = password;
        }

        protected override Packet initByJSON(MemoryStream stream)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(PacketAuthorization));
            Packet packet = (Packet)jsonFormatter.ReadObject(stream);
            packet.m_type = m_type;
            return packet;
        }

        public override void onReceive()
        {
            System.Diagnostics.Debug.WriteLine("onReceive " + " m_result " + m_result);
            ApplicationShared.instance().getUser().onLoginResult(m_phone, m_password, m_result);
        }
    }
}
