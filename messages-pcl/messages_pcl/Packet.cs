﻿using System;
using System.Linq;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;



namespace messages_pcl
{
    public enum PACKET_TYPES
    {
        AUTHORIZATION,
        GET_PASSWORD,
        MESSAGE,
        PING,
    }

    [DataContract]
    public class Packet
    {
        private static int PACKET_TYPE_SIZE = 4;
        private static int PACKET_MSG_SIZE = 4;

        public int m_type;

        public Packet(int typeId)
        {
            m_type = typeId;
        }

        public static Packet build(PACKET_TYPES type)
        {
            Packet packet;
            switch (type)
            {
                case PACKET_TYPES.AUTHORIZATION:
                    {
                        packet = new PacketAuthorization((int)type);
                        break;
                    }
                case PACKET_TYPES.GET_PASSWORD:
                    {
                        packet = new PacketGetPassword((int)type);
                        break;
                    }
                case PACKET_TYPES.MESSAGE:
                    {
                        packet = new PacketMessage((int)type);
                        break;
                    }
                default:
                    {
                        packet = new Packet((int)type);
                        break;
                    }
            }
            return packet;
        }

        public static bool isReceive(byte[] buf, int readed)
        {
            if (readed < 4)
                return false;
            
            var sizeq = BitConverter.ToInt32(buf, 0);
            System.Diagnostics.Debug.WriteLine("Прочитано build PACKET_TYPES.MESSAGE ++ " + " sizeq " + sizeq + " readed " + readed);

            byte[] buffer = new byte[4];
            buffer[0] = buf[3]; buffer[1] = buf[2]; buffer[2] = buf[1]; buffer[3] = buf[0];
            var size = BitConverter.ToInt32(buffer, 0);
            System.Diagnostics.Debug.WriteLine("Прочитано build PACKET_TYPES.MESSAGE ++ " + " size " + size + " readed " + readed);
            return (size == readed);

        }

        public static Packet build(byte[] buf, int readed)
        {
            Packet packet = null;
            try
            {
                byte[] buffer = new byte[4];
                buffer[0] = buf[PACKET_MSG_SIZE + 3]; buffer[1] = buf[PACKET_MSG_SIZE + 2]; buffer[2] = buf[PACKET_MSG_SIZE + 1]; buffer[3] = buf[PACKET_MSG_SIZE + 0];
                var type = BitConverter.ToInt32(buffer, 0);
                
                System.Diagnostics.Debug.WriteLine("Прочитано build PACKET_TYPES.MESSAGE ++ " + " type " + type + " packet ");
                packet = build((PACKET_TYPES) type);

                MemoryStream stream = new MemoryStream(buf.Skip(PACKET_MSG_SIZE + PACKET_TYPE_SIZE).Take(readed - PACKET_MSG_SIZE - PACKET_TYPE_SIZE).ToArray<byte>());
                packet = packet.initByJSON(stream);
                //packet.onReceive();

                //Console.Out.WriteLine("line.." + System.Text.Encoding.UTF8.GetString(packet.getRequestJSON()) + "..line");
            }
            catch (ArgumentNullException)
            {

            }
            return packet;
        }

        protected virtual Packet initByJSON(MemoryStream stream)
        {
            return this;
        }

        public virtual void onReceive()
        {
        }


        public byte[] getRequestJSON()
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(this.GetType());

            MemoryStream stream = new MemoryStream();
            jsonFormatter.WriteObject(stream, this);
            byte[] data = new byte[PACKET_TYPE_SIZE + PACKET_MSG_SIZE + stream.Length];

            unchecked
            {
                data[0] = (byte)((Int32)m_type >> 24);
                data[1] = (byte)((Int32)m_type >> 16);
                data[2] = (byte)((Int32)m_type >> 8);
                data[3] = (byte)((Int32)m_type);
            }

            unchecked
            {
                data[PACKET_TYPE_SIZE + 0] = (byte)((Int32)stream.Length >> 24);
                data[PACKET_TYPE_SIZE + 1] = (byte)((Int32)stream.Length >> 16);
                data[PACKET_TYPE_SIZE + 2] = (byte)((Int32)stream.Length >> 8);
                data[PACKET_TYPE_SIZE + 3] = (byte)((Int32)stream.Length);
            }
            //Console.Out.Write("stream.GetBuffer().Length " + stream.Length + " length " + stream.Position + "\n");
            //Console.Out.WriteLine("stream.GetBuffer()..." + System.Text.Encoding.UTF8.GetString(stream.GetBuffer()) + "...");
            //stream.(data, (int)stream.Length);
            //System.Diagnostics.Debug.WriteLine("Отправляем " + data.Length + " data " + System.Text.Encoding.UTF8.GetString(data, 0, data.Length));

            stream.Position = 0;
            stream.Read(data, PACKET_TYPE_SIZE + PACKET_MSG_SIZE, (int) stream.Length);
            return data;
        }
    }
}
