﻿using System;

using System.IO;
using System.Threading.Tasks;
/*
using System.Threading;
using System.Net;
using System.Net.Sockets;*/

using Sockets.Plugin;
using Sockets.Plugin.Abstractions;

namespace messages_pcl
{
    public class SocketClient
    {
        private static int BUFFER_SIZE = 16192;
        private static SocketClient m_instance;

        private SocketConnectionStaus m_socket_connection_status;
        private int m_channel_id;
        private TcpSocketClient m_tcp_client;
        private Stream m_stream_read;
        private Stream m_stream_write;
        private byte[] m_buf = new byte[BUFFER_SIZE];
        

        public async Task<bool> connect()
        {
            System.Diagnostics.Debug.WriteLine("connect...1");
            m_tcp_client = new TcpSocketClient();
            System.Diagnostics.Debug.WriteLine("connect...2");
            try
            {
                await m_tcp_client.ConnectAsync("188.120.227.190", 9132);
            }
            catch (SocketException e)
            {
                
                return false;
            }
            System.Diagnostics.Debug.WriteLine("connect...3");
            m_stream_read = m_tcp_client.ReadStream;
            System.Diagnostics.Debug.WriteLine("connect...4");
            m_stream_write = m_tcp_client.WriteStream;
            System.Diagnostics.Debug.WriteLine("connect...5");

            if (isConnected())
                m_socket_connection_status.onConnect();

            return isConnected();
        }

        public async void disconnect()
        {
            bool beforeIsConnected = isConnected();
            await m_tcp_client.DisconnectAsync();

            if (beforeIsConnected)
                m_socket_connection_status.onDisconnect();
        }

        public bool isConnected()
        {
            if (m_stream_read != null & m_stream_write != null)
                return true;
            return false;
        }


        public async void startReceive()
        {
            System.Diagnostics.Debug.WriteLine("line...startReceive");
            //Packet packet = Packet.build(PACKET_TYPES.AUTHORIZATION);
            //send(packet);
            while (true)
            {
                if (m_stream_read != null)
                    receive();

                System.Threading.Tasks.Task.Delay(10).Wait();
            }
        }

        private void send(Packet packet)
        {
            byte[] data = packet.getRequestJSON();

            System.Diagnostics.Debug.WriteLine("Отправляем " + data.Length + " data " + System.Text.Encoding.UTF8.GetString(data, 0, data.Length));

            if (m_stream_write == null)
                return;

            m_stream_write.Write(data, 0, data.Length);
        }

        private void receive()
        {
            int readed = 0;
            int readedIter = 0;
            while ((readedIter = m_stream_read.Read(m_buf, readed, BUFFER_SIZE - readed)) != -1)
            {
                readed += readedIter;
                System.Diagnostics.Debug.WriteLine("Прочитано " + readedIter);

                if (Packet.isReceive(m_buf, readed))
                {
                    System.Diagnostics.Debug.WriteLine("!!!Прочитано " + readed + " m_stream.Length " + m_buf.Length);
                    System.Diagnostics.Debug.WriteLine("!!!Прочитано " + System.Text.Encoding.UTF8.GetString(m_buf, 8, readed - 8));

                    Packet.build(m_buf, readed).onReceive();
                    readed = 0;
                    break;
                }
            }
        }


        public void login(String phone, String code, String password)
        {
            if (phone == null || code == null || password == null)
                return;

            PacketAuthorization packet = (PacketAuthorization) Packet.build(PACKET_TYPES.AUTHORIZATION);
            packet.setPhone(phone);
            packet.setCode(code);
            packet.setPassword(password);

            send(packet);
        }

        public void getPassword(String phone, String code)
        {
            System.Diagnostics.Debug.WriteLine("getPassword 1");
            PacketGetPassword packet = (PacketGetPassword) Packet.build(PACKET_TYPES.GET_PASSWORD);
            System.Diagnostics.Debug.WriteLine("getPassword 2");
            packet.setPhone(phone);
            System.Diagnostics.Debug.WriteLine("getPassword 3");
            packet.setCode(code);
            System.Diagnostics.Debug.WriteLine("getPassword 4");

            send(packet);
            System.Diagnostics.Debug.WriteLine("getPassword 5");
        }

        public void setSocketConnectionStatus(SocketConnectionStaus socketConnectionStatus)
        {
            m_socket_connection_status  = socketConnectionStatus;/*

            if (isConnected())
                m_socket_connection_status.onConnect();
            else
                m_socket_connection_status.onDisconnect();*/
        }
    }

    public interface SocketConnectionStaus
    {
        void onConnect();
        void onDisconnect();
    }
}