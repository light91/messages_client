﻿using System;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite.Net.Attributes;

namespace messages_pcl
{
    [DataContract]
    public class MessageItem
    {
        [PrimaryKey]
        [DataMember(Name="m_id")]
        public String Id { get; private set; }
        [DataMember(Name = "m_title")]
        public String Title { get; private set; }
        [DataMember(Name = "m_desc")]
        public String Desc { get; private set; }
        [DataMember(Name = "m_date")]
        public String Date { get; private set; }
        [DataMember(Name = "m_type")]
        public String Type { get; private set; }
        [DataMember(Name = "m_html")]
        public String Html { get; private set; }
        // Было ли сообщение прочитано пользователем
        public bool Readed { get; private set; }
        // Пользователь пока только кликнул на checkbox сообщения в режиме редактирования, но не подтвердил удаление
        public bool m_is_remove_prepare;


        public static MessageItem initByJSON(Stream stream)
        {
            System.Diagnostics.Debug.WriteLine("!!!MessageItem initByJSON 1");
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(MessageItem));
            var messageItem = (MessageItem) jsonFormatter.ReadObject(stream);

            System.Diagnostics.Debug.WriteLine("!!!MessageItem initByJSON Id " + messageItem.Id);
            byte[] html = Convert.FromBase64String(messageItem.Html);
            System.Diagnostics.Debug.WriteLine("!!!MessageItem initByJSON 2");
            System.Diagnostics.Debug.WriteLine("!!!MessageItem initByJSON 2 " + messageItem.Html);
            messageItem.Html = System.Text.Encoding.GetEncoding("UTF-8").GetString(html, 0, html.Length);
            System.Diagnostics.Debug.WriteLine("!!!MessageItem initByJSON 2 " + messageItem.Html);

            return messageItem;
        }


        public void setReaded()
        {
            Readed = true;
            MessageManager.instance().setReaded(this);
        }

        public void SetRemovedPrepare(bool isRemovedPrepare)
        {
            m_is_remove_prepare = isRemovedPrepare;
        }

        public bool IsRemovedPrepare()
        {
            return m_is_remove_prepare;
        }
    }
}