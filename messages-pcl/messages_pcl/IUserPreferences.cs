﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace messages_pcl
{
    public interface IUserPreferences
    {
        void SetString(string key, string value);
        string GetString(string key);
    }
}
