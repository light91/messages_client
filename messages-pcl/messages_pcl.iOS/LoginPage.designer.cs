﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace messages_pcl.iOS
{
    [Register ("LoginPage")]
    partial class LoginPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Login { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Password { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PasswordRestore { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Phone { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Login != null) {
                Login.Dispose ();
                Login = null;
            }

            if (Password != null) {
                Password.Dispose ();
                Password = null;
            }

            if (PasswordRestore != null) {
                PasswordRestore.Dispose ();
                PasswordRestore = null;
            }

            if (Phone != null) {
                Phone.Dispose ();
                Phone = null;
            }
        }
    }
}