﻿using Foundation;
using System;
using UIKit;
using System.IO;

namespace messages_pcl.iOS
{
    public partial class TableViewControllerItem : UIViewController
    {
        private MessageItem m_item;


        public TableViewControllerItem (IntPtr handle) : base (handle)
        {
        }

        public void setData(MessageItem item)
        {
            m_item = item;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            String contentDirectoryPath = Path.Combine(NSBundle.MainBundle.BundlePath, "Content/");
            WebView.LoadHtmlString(m_item.Html, new NSUrl(contentDirectoryPath, true));
        }
    }
}