using System;

using Foundation;
using UIKit;

namespace messages_pcl.iOS
{
    public partial class LoginPage : UIViewController, OnLoginReuslt
    {
        public static readonly NSString Key = new NSString("LoginPage");
        public static readonly UINib Nib;

        static LoginPage()
        {
            Nib = UINib.FromName("LoginPage", NSBundle.MainBundle);
        }

        protected LoginPage(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            ApplicationExt.instance().getUser().m_login_result = this;

            Login.TouchUpInside += delegate
            {
                ApplicationExt.instance().getSocketClient().login(Phone.Text, ApplicationExt.instance().getPhoneId(), Password.Text);
            };

            PasswordRestore.TouchUpInside += delegate
            {
                System.Diagnostics.Debug.WriteLine("PasswordRestore 0");

                var t = ApplicationExt.instance().getSocketClient();
                System.Diagnostics.Debug.WriteLine("PasswordRestore 1 " + (t != null) + " Phone.Text " + (Phone.Text != null));

                t.getPassword(Phone.Text, ApplicationExt.instance().getPhoneId());
                System.Diagnostics.Debug.WriteLine("PasswordRestore 2");
            };
        }


        public void OpenMainPage()
        {
            InvokeOnMainThread(() =>
            {
                MainPage page = Storyboard.InstantiateViewController("MainPage") as MainPage;
                NavigationController.PushViewController(page, true);
            });
        }
        
        public void onLoginResult(bool result)
        {
            System.Diagnostics.Debug.WriteLine("onLoginResult result: " + result);
            if (result)
                OpenMainPage();
        }
    }
}