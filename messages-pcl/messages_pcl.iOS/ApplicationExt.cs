using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;
using SQLite.Net.Platform.XamarinIOS;
using System.Threading;

namespace messages_pcl.iOS
{
    [Register ("ApplicationExt")]
    class ApplicationExt : UIApplication
    {
        private static ApplicationExt m_instance;
        private SocketClientAdapter m_socket_client;


        public ApplicationExt() : base ()
        {
            initialize();
        }

        public ApplicationExt(IntPtr handle) : base (handle)
        {
            initialize();
        }

        public ApplicationExt(Foundation.NSObjectFlag t) : base (t)
        {
            initialize();
        }
        

        public static ApplicationExt instance()
        {
            return m_instance;
        }


        public void initialize()
        {
            m_instance = this;
            System.Diagnostics.Debug.WriteLine("pre initialize");

            System.Diagnostics.Debug.WriteLine("initialize");
            /*try
            {*/
                var path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Messages.db");
                ApplicationShared.instance().initialize(new IOSUserPreferences(), new SQLitePlatformIOS(), path);
            /*}
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("e.Mess " + e.Message + " GetType " + e.GetType());
            }*/
            System.Diagnostics.Debug.WriteLine("post initialize");
            new Thread(() =>
            {
                System.Diagnostics.Debug.WriteLine("m_socket_client 0 " + (m_socket_client != null));
                m_socket_client = new SocketClientAdapter();
                System.Diagnostics.Debug.WriteLine("m_socket_client 1 " + (m_socket_client != null));
                m_socket_client.SocketConnect();
                System.Diagnostics.Debug.WriteLine("m_socket_client 2 " + (m_socket_client != null));
            }).Start();
        }


        public SocketClientAdapter getSocketClient()
        {
            System.Diagnostics.Debug.WriteLine("m_socket_client 3 " + (m_socket_client != null));
            return m_socket_client;
        }

        public User getUser()
        {
            return ApplicationShared.instance().getUser();
        }

        public String getPhoneId()
        {
            return "DD";
        }
    }
}