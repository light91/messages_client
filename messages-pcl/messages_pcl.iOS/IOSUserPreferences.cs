using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;

namespace messages_pcl.iOS
{
    class IOSUserPreferences : IUserPreferences
    {
        public void SetString(string key, string value)
        {
            System.Diagnostics.Debug.WriteLine("SetString " + key + " value " + value);
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetString(value, key);
            plist.Synchronize();
        }

        public string GetString(string key)
        {
            var plist = NSUserDefaults.StandardUserDefaults;
            System.Diagnostics.Debug.WriteLine("GetString " + key + " plist.StringForKey(key) " + plist.StringForKey(key));
            return plist.StringForKey(key);
        }
    }
}