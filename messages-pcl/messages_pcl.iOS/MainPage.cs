﻿using System;
using Foundation;
using UIKit;

namespace messages_pcl.iOS
{
    public partial class MainPage : UIViewController
    {
        private UIView m_mesages_view;
        private UIView m_commands_view;
        private UIView m_connections_view;


        public MainPage (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            var controller = Storyboard.InstantiateViewController("Messages");
            AddChildViewController(controller);
            m_mesages_view = controller.View;
            Container.AddSubview(m_mesages_view);

            controller = Storyboard.InstantiateViewController("Commands");
            AddChildViewController(controller);
            m_commands_view = controller.View;
            Container.AddSubview(m_commands_view);

            controller = Storyboard.InstantiateViewController("Connections");
            AddChildViewController(controller);
            m_connections_view = controller.View;
            Container.AddSubview(m_connections_view);


            MessagesButton.TouchUpInside += delegate
            {
                openTab(MessagesButton, m_mesages_view);
            };

            CommandsButton.TouchUpInside += delegate
            {
                openTab(CommandsButton, m_commands_view);
            };

            ConnectionsButton.TouchUpInside += delegate
            {
                openTab(ConnectionsButton, m_connections_view);
            };
            
            openTab(MessagesButton, m_mesages_view);
        }


        private void openTab(UIButton label, UIView view)
        {
            System.Diagnostics.Debug.WriteLine("openTab");
            m_mesages_view.Hidden = true;
            m_commands_view.Hidden = true;
            m_connections_view.Hidden = true;

            view.Hidden = false;
            //label;
        }



		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
    }
}

