﻿using Foundation;
using System;
using UIKit;

namespace messages_pcl.iOS
{
    public partial class TableViewController : UIViewController
    {
        public TableViewController (IntPtr handle) : base (handle)
        {
            System.Diagnostics.Debug.WriteLine("TableViewController 1 + m_controller " + (this != null) + " m_controller.NavigationController " + (this.NavigationController != null));
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            System.Diagnostics.Debug.WriteLine("TableViewController 2 + m_controller " + (this != null) + " m_controller.NavigationController " + (this.NavigationController != null));

            System.Diagnostics.Debug.WriteLine("ParentViewController& 3 + page " + (ParentViewController != null));


            Table.Source = new TableSource(this, Table);
            Table.ReloadData();
        }
    }
}