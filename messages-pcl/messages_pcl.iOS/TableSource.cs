﻿using Foundation;
using System;
using UIKit;

namespace messages_pcl.iOS
{
    public partial class TableSource : UITableViewSource, OnNotifyDataChanged
    {
        private UIViewController m_controller;
        private UITableView m_table;
        string CellIdentifier = "TableCell";

        public TableSource(UIViewController Controller, UITableView Table)
        {
            MessageManager.instance().AddNotifyDataChangedListener(this);
            m_controller = Controller;
            m_table = Table;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return MessageManager.instance().getCount();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            MessageItem item = MessageManager.instance().get(indexPath.Row);
            TableViewCell cell = (TableViewCell) tableView.DequeueReusableCell(CellIdentifier);

            if (cell == null)
                cell = new TableViewCell(UITableViewCellStyle.Default, CellIdentifier);

            cell.setData(item);
            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            System.Diagnostics.Debug.WriteLine("ParentViewController& 3 + page " + (m_controller.ParentViewController != null));

            System.Diagnostics.Debug.WriteLine("RowSelected 1");
            MessageItem item = MessageManager.instance().get(indexPath.Row);
            System.Diagnostics.Debug.WriteLine("RowSelected 2");

            TableViewControllerItem page = m_controller.Storyboard.InstantiateViewController("TableViewControllerItem") as TableViewControllerItem;
            System.Diagnostics.Debug.WriteLine("RowSelected 3 + page " + (page != null));
            page.setData(item);
            System.Diagnostics.Debug.WriteLine("RowSelected 3 + m_controller " + (m_controller != null) + " m_controller.NavigationController " + (m_controller.NavigationController != null));
            try
            {
                m_controller.NavigationController.PushViewController(page, true);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("e " + e.Message + " e " + e.GetType());
            }
            System.Diagnostics.Debug.WriteLine("RowSelected 4");
        }


        public void OnNotifyDataChanged()
        {
            m_table.ReloadData();
        }
        /*
        public override View GetView(int position, View view, ViewGroup parent)
        {
            if (view == null)
                view = (ApplicationExt.instance().GetSystemService(Context.LayoutInflaterService) as LayoutInflater).Inflate(Resource.Layout.MessageItemAdapter, parent, false);

            MessageItem item = this[position];

            view.FindViewById<TextView>(Resource.Id.Title).SetText(item.Title, TextView.BufferType.Normal);
            view.FindViewById<TextView>(Resource.Id.Date).SetText(item.Date, TextView.BufferType.Normal);
            view.FindViewById<TextView>(Resource.Id.Desc).SetText(item.Desc, TextView.BufferType.Normal);
            view.FindViewById<TextView>(Resource.Id.Type).SetText(item.Type, TextView.BufferType.Normal);

            view.FindViewById(Resource.Id.bownlish_line).Visibility = item.Readed ? ViewStates.Gone : ViewStates.Visible;
            view.FindViewById<TextView>(Resource.Id.Title).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Date).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Desc).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Type).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            //view.FindViewById(Resource.Id.message_layout).SetTag(item.getId(), TAG_ID);

            return view;
        }*/
    }
}
