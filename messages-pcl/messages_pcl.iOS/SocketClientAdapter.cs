using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace messages_pcl.iOS
{
    public class SocketClientAdapter : SocketConnectionStaus
    {
        private SocketClient m_adaptee;
        private Thread m_thread_socket_receive;
        private SocketConnectionStaus m_socket_connection_status;

        public SocketClientAdapter()
        {
            m_adaptee = new SocketClient();
            m_adaptee.setSocketConnectionStatus(this);
            //SocketConnect();
        }



        public void SocketConnect()
        {
            /*
            new Thread(() =>
            {
                while (!connect())
                {
                    System.Threading.Tasks.Task.Delay(1000 * 30).Wait();
                }
                receiveMessages();
            }).Start();
            */
            while (!connect())
            {
                System.Threading.Tasks.Task.Delay(1000 * 30).Wait();
            }
            receiveMessages();
        }


        public bool connect()
        {
            return m_adaptee.connect().Result;
        }

        public void disconnect()
        {
            m_adaptee.disconnect();
        }

        public void setSocketConnectionStatus(SocketConnectionStaus socketConnectionStatus)
        {
            m_socket_connection_status = socketConnectionStatus;
        }

        public bool isConnected()
        {
            return m_adaptee.isConnected();
        }

        public void receiveMessages()
        {
            m_thread_socket_receive = new Thread(delegate ()
            {
                m_adaptee.startReceive();
            });
            m_thread_socket_receive.Start();
        }


        public void login(String phone, String code, String password)
        {
            m_adaptee.login(phone, code, password);
        }

        public void getPassword(String phone, String code)
        {
            System.Diagnostics.Debug.WriteLine("getPassword 0");
            m_adaptee.getPassword(phone, code);
        }

        void SocketConnectionStaus.onConnect()
        {
            if (m_socket_connection_status != null)
                m_socket_connection_status.onConnect();
        }

        void SocketConnectionStaus.onDisconnect()
        {
            if (m_socket_connection_status != null)
                m_socket_connection_status.onDisconnect();
            SocketConnect();
        }
    }
}