﻿using Foundation;
using System;
using UIKit;
using System.Threading;

namespace messages_pcl.iOS
{
    public partial class LogoPage : UIViewController, SocketConnectionStaus, OnLoginReuslt
    {
        private Timer m_timer;
        private Thread m_sending_thread;

        public LogoPage (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            TimerCallback timerCallback = new TimerCallback(OpenLoginPage);
            m_timer = new Timer(timerCallback, null, 5000, 0);

            trySendRequest();
        }

        private void trySendRequest()
        {
            ApplicationExt.instance().getUser().m_login_result = this;
            m_sending_thread = new Thread(() =>
            {
                System.Diagnostics.Debug.WriteLine("trySendRequest 1");
                while (ApplicationExt.instance().getSocketClient() == null || ApplicationExt.instance().getSocketClient().isConnected() == false)
                    System.Threading.Tasks.Task.Delay(300).Wait();

                System.Diagnostics.Debug.WriteLine("trySendRequest 2");

                //ApplicationExt.instance().getSocketClient().setSocketConnectionStatus(this);
                onConnect();
            });
            m_sending_thread.Start();
        }

        private void stopSendingRequest()
        {
            m_sending_thread.Abort();
            m_sending_thread = null;
        }

        private void stopTimer()
        {
            m_timer.Dispose();
            m_timer = null;
        }


        public void OpenLoginPage(Object param)
        {
            stopSendingRequest();

            InvokeOnMainThread(() =>
            {
                LoginPage page = Storyboard.InstantiateViewController("LoginPage") as LoginPage;
                NavigationController.PushViewController(page, true);
            });
        }

        public void OpenMainPage()
        {
            stopTimer();

            InvokeOnMainThread(() =>
            {
                MainPage page = Storyboard.InstantiateViewController("MainPage") as MainPage;
                NavigationController.PushViewController(page, true);
            });
        }


        public void onConnect()
        {
            System.Diagnostics.Debug.WriteLine("trySendRequest 3");
            User user = ApplicationExt.instance().getUser();
            System.Diagnostics.Debug.WriteLine("trySendRequest 4");
            ApplicationExt.instance().getSocketClient().login(user.getPhone(), ApplicationExt.instance().getPhoneId(), user.getPassword());
        }

        public void onDisconnect()
        {

        }

        public void onLoginResult(bool result)
        {
            System.Diagnostics.Debug.WriteLine("onLoginResult ");
            if (result)
                OpenMainPage();
        }
    }
}