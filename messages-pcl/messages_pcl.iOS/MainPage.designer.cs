﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace messages_pcl.iOS
{
    [Register ("MainPage")]
    partial class MainPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CommandsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ConnectionsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Container { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton MessagesButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CommandsButton != null) {
                CommandsButton.Dispose ();
                CommandsButton = null;
            }

            if (ConnectionsButton != null) {
                ConnectionsButton.Dispose ();
                ConnectionsButton = null;
            }

            if (Container != null) {
                Container.Dispose ();
                Container = null;
            }

            if (MessagesButton != null) {
                MessagesButton.Dispose ();
                MessagesButton = null;
            }
        }
    }
}