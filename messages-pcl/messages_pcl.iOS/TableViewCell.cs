﻿using Foundation;
using System;
using UIKit;

namespace messages_pcl.iOS
{
    public partial class TableViewCell : UITableViewCell
    {
        public TableViewCell(IntPtr handle) : base(handle)
        {
        }

        public TableViewCell(UITableViewCellStyle style, string reuseIdentifier) : base(style, reuseIdentifier)
        {

        }

        public TableViewCell(UITableViewCellStyle style, NSString reuseIdentifier) : base(style, reuseIdentifier)
        {
        }


        public void setData(MessageItem item)
        {
            Title.Text = item.Title;
            Desc.Text = item.Desc;
            Date.Text = item.Date;
            Type.Text = item.Type;
        }
    }
}