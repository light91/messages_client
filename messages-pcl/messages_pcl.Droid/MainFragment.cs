using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace messages_pcl.Droid
{
    public class MainFragment : Fragment, OnNotifyDataChanged, CheckPrepareFragmentBack
    {
        private static String TAG_TAB_MESSAGES = "TAG_TAB_MESSAGES";
        private static String TAG_TAB_COMMANDS = "TAG_TAB_COMMANDS";
        private static String TAG_TAB_CONNECTS = "TAG_TAB_CONNECTS";

        private TabHost m_tab_host;
        private TabHost.TabSpec m_tab_messages;

        String ttt = "0000000000";


        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);


            Activity.FindViewById(Resource.Id.RemoveSelectedItems).Click += delegate
            {
                MessageManager.instance().removeAllPrepareItems();
                ListView messageList = m_tab_host.FindViewById<ListView>(Resource.Id.MessageList);
                (messageList.Adapter as MessagesListAdapter).openViewMode();
            };
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View mainView = inflater.Inflate(Resource.Layout.MainFragment, container, false);


            m_tab_host = mainView.FindViewById<TabHost>(Android.Resource.Id.TabHost);
            m_tab_host.Setup();

            TabHost.TabSpec tabSpec = m_tab_host.NewTabSpec(TAG_TAB_MESSAGES);
            tabSpec.SetContent(Resource.Id.tab1);
            m_tab_messages = tabSpec;
            updateMsgTabText();
            ListView messageList = m_tab_host.FindViewById<ListView>(Resource.Id.MessageList);
            messageList.SetAdapter(new MessagesListAdapter(Activity));
            messageList.ItemClick += (sender, args) =>
            {
                openMessageItem(args.Position);
            };
            messageList.OnItemLongClickListener = new LongClickListener(messageList);
            m_tab_host.AddTab(tabSpec);

            View view = ((LayoutInflater)Application.Context.GetSystemService(Context.LayoutInflaterService)).Inflate(Resource.Layout.MenuIndicator, null);
            view.FindViewById<TextView>(Resource.Id.text).SetText(GetText(Resource.String.main_tab_commands), TextView.BufferType.Normal);
            tabSpec = m_tab_host.NewTabSpec(TAG_TAB_COMMANDS);
            tabSpec.SetContent(Resource.Id.tab2);
            tabSpec.SetIndicator(view);
            m_tab_host.AddTab(tabSpec);

            view = ((LayoutInflater)Application.Context.GetSystemService(Context.LayoutInflaterService)).Inflate(Resource.Layout.MenuIndicator, null);
            view.FindViewById<TextView>(Resource.Id.text).SetText(GetText(Resource.String.main_tab_connects), TextView.BufferType.Normal);
            tabSpec = m_tab_host.NewTabSpec(TAG_TAB_CONNECTS);
            tabSpec.SetContent(Resource.Id.tab3);
            tabSpec.SetIndicator(view);
            m_tab_host.AddTab(tabSpec);

            m_tab_host.SetCurrentTabByTag(TAG_TAB_MESSAGES);
            MessageManager.instance().AddNotifyDataChangedListener(this);

            return mainView;
        }

        public override void OnStart()
        {
            base.OnStart();

            ttt = "1234567890";
            BaseAdapter adapter = (BaseAdapter) m_tab_host.FindViewById<ListView>(Resource.Id.MessageList).Adapter;
            adapter.NotifyDataSetChanged();
        }

        private void updateMsgTabText()
        {
            View view = ((LayoutInflater)Application.Context.GetSystemService(Context.LayoutInflaterService)).Inflate(Resource.Layout.MenuIndicator, null);
            int messagesCount = MessageManager.instance().getCount();
            String tabNameStr = GetText(Resource.String.main_tab_messages);
            SpannableStringBuilder tagName = new SpannableStringBuilder(tabNameStr);
            if (messagesCount != 0)
            {
                var messagesCountStr = Html.FromHtml(" <sup>" + messagesCount + "</sup>");
                tagName.Append(messagesCountStr);
                tagName.SetSpan(new RelativeSizeSpan(0.62f), tabNameStr.Length, tabNameStr.Length + messagesCountStr.Length(), SpanTypes.ExclusiveExclusive);
            }
            ((TextView)view).SetText(tagName, TextView.BufferType.Spannable);
            m_tab_messages.SetIndicator(view);
        }

        public void openMessageItem(long messageIndex)
        {
            Fragment fragment = MessageItemFragment.instance((int)messageIndex); 
            (Activity as MainActivity).fragmentForward(fragment);
        }

        public void OnNotifyDataChanged()
        {
            updateMsgTabText();
        }

        public bool checkPrepareFragmentBack()
        {
            System.Diagnostics.Debug.WriteLine(" checkPrepareFragmentBack ttt " + ttt);
            MessagesListAdapter adapter = (MessagesListAdapter) m_tab_host.FindViewById<ListView>(Resource.Id.MessageList).Adapter;
            System.Diagnostics.Debug.WriteLine(" adapter.isEditMode() " + adapter.isEditMode());
            if (adapter.isEditMode())
            {
                adapter.openViewMode();
                return false;
            }
            return true;
        }
    }

    class LongClickListener : Java.Lang.Object, AdapterView.IOnItemLongClickListener
    {
        private ListView m_messages_list;

        public LongClickListener(ListView messageList)
        {
            m_messages_list = messageList;
        }


        public bool OnItemLongClick(AdapterView parent, View view, int position, long id)
        {
            System.Diagnostics.Debug.WriteLine("LongClick");
            (m_messages_list.Adapter as MessagesListAdapter).openEditMode();
            return true;
        }
    }
}