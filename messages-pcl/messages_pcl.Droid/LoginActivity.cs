using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Telephony;
using static Android.Views.View;
using Android.Text;

namespace messages_pcl.Droid
{
    [Activity(MainLauncher = true, Exported = true)]
    public class LoginActivity : Activity, SocketConnectionStaus, OnLoginReuslt
    {
        private Timer m_timer;
        public static String MASK_PHONE_NUMBER = "7(___) ___ __ __";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);

            TimerCallback timerCallback = new TimerCallback(VisLoginPage);
            m_timer = new Timer(timerCallback, null, 4000, 0);


            EditText phone = FindViewById<EditText>(Resource.Id.PhoneEdit);
            EditText password = FindViewById<EditText>(Resource.Id.PasswordEdit);
            Button getPassword = FindViewById<Button>(Resource.Id.get_password);
            Button login = FindViewById<Button>(Resource.Id.login);

            phone.Text = MASK_PHONE_NUMBER;
            phone.AddTextChangedListener(new MaskTextWatcher(phone));

            getPassword.Click += delegate
            {
                FindViewById(Resource.Id.PasswordText).Visibility = ViewStates.Visible;
                password.Visibility = ViewStates.Visible;
                login.Visibility = ViewStates.Visible;

                StringBuilder builder = new StringBuilder();
                String phoneText = phone.Text;
                for (int index = 0; index != phoneText.Length; ++index)
                    if (Char.IsNumber(phoneText.ToArray()[index]))
                        builder.Append(phoneText.ToArray()[index]);
                phoneText = builder.ToString();

                ApplicationExt.instance().getSocketClient().getPassword(phoneText, ApplicationExt.instance().getPhoneId());
                Toast.MakeText(this, "��� ���������", ToastLength.Short).Show();
            };
            login.Click += delegate
            {
                StringBuilder builder = new StringBuilder();
                String phoneText = phone.Text;
                for (int index = 0; index != phoneText.Length; ++index)
                    if (Char.IsNumber(phoneText.ToArray()[index]))
                        builder.Append(phoneText.ToArray()[index]);
                phoneText = builder.ToString();

                String passwordText = password.Text;
                ApplicationExt.instance().getSocketClient().login(phoneText, ApplicationExt.instance().getPhoneId(), passwordText);
            };

            ApplicationExt.instance().getUser().m_login_result = this;
            new Thread(() =>
            {
                while (ApplicationExt.instance().getSocketClient() == null)
                    System.Threading.Tasks.Task.Delay(300).Wait();

                ApplicationExt.instance().getSocketClient().setSocketConnectionStatus(this);
            }).Start();

            VisLogoPage();
            onLoginResult(true);
        }


        public void VisLogoPage()
        {
            FindViewById(Resource.Id.Logo).Visibility = ViewStates.Visible;
            FindViewById(Resource.Id.Login).Visibility = ViewStates.Gone;
        }


        public void VisLoginPage(Object param)
        {
            RunOnUiThread(() =>
            {
                FindViewById(Resource.Id.Logo).Visibility = ViewStates.Gone;
                FindViewById(Resource.Id.Login).Visibility = ViewStates.Visible;
            });
        }


        public void onConnect()
        {
            User user = ApplicationExt.instance().getUser();
            ApplicationExt.instance().getSocketClient().login(user.getPhone(), ApplicationExt.instance().getPhoneId(), user.getPassword());
        }

        public void onDisconnect()
        {

        }

        public void onLoginResult(bool result)
        {
            System.Diagnostics.Debug.WriteLine("onLoginResult ");
            if (result)
                StartActivity(typeof(MainActivity));
        }
    }


    public class MaskTextWatcher : Java.Lang.Object, ITextWatcher
    {
        private EditText m_phone;
        private int m_selected_index;
        private String m_before_text_changed;
        private bool m_skip_check_changed;

        public MaskTextWatcher(EditText phone)
        {
            m_phone = phone;
        }


        public void AfterTextChanged(IEditable s)
        {
        }

        public void BeforeTextChanged(Java.Lang.ICharSequence s, int start, int count, int after)
        {
            m_selected_index = m_phone.SelectionEnd;
            m_before_text_changed = s.ToString();
        }

        public void OnTextChanged(Java.Lang.ICharSequence s, int start, int before, int count)
        {
            if (m_skip_check_changed)
            {
                m_skip_check_changed = false;
                return;
            }

            var textBeforeChanged = m_before_text_changed.ToArray();
            if (count < before)
            {
                Char symbolRemoved = textBeforeChanged[m_selected_index - 1];
                if (Char.IsNumber(symbolRemoved) || symbolRemoved == '_' || symbolRemoved == '(' || symbolRemoved == ')' || symbolRemoved == ' ')
                {
                    m_skip_check_changed = true;

                    if (Char.IsNumber(symbolRemoved) && (m_selected_index - 1) != 0)
                        textBeforeChanged[m_selected_index - 1] = '_';
                    m_phone.Text = new String(textBeforeChanged);
                    m_phone.SetSelection(m_selected_index);
                }
            }
            else if (count > before)
            {
                if (m_before_text_changed.Length != m_selected_index)
                {
                    char symbolPrev = m_before_text_changed.ToArray()[m_selected_index];
                    char symbolAdded = s.ToArray()[m_selected_index];
                    if (Char.IsNumber(symbolAdded))
                    {
                        m_skip_check_changed = true;

                        int selectionIndex = m_selected_index + 1;
                        for (int index = m_selected_index; index != textBeforeChanged.Length; ++index)
                        {
                            if (Char.IsNumber(textBeforeChanged[index]) || textBeforeChanged[index] == '_')
                            {
                                selectionIndex = index + 1;
                                textBeforeChanged[index] = symbolAdded;
                                break;
                            }
                        }
                        m_phone.Text = new String(textBeforeChanged);
                        m_phone.SetSelection(selectionIndex);
                    }
                }
                else
                {
                    m_skip_check_changed = true;
                    m_phone.Text = new String(textBeforeChanged);
                    m_phone.SetSelection(m_selected_index - 1);
                }
            }
        }
    }
}