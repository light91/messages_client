using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

using System.Threading;
using SQLite.Net.Platform.XamarinAndroid;
using System.IO;

namespace messages_pcl.Droid
{
    [Application]
    public class ApplicationExt : Application, IServiceConnection
    {
        private static ApplicationExt m_instance;
        private SocketClientAdapter m_socket_client;


        public ApplicationExt() : base()
        {
            initialize();
        }

        public ApplicationExt(IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip)
        {
            initialize();
        }

        public static ApplicationExt instance()
        {
            return m_instance;
        }

        public void initialize()
        {
            m_instance = this;

            var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            path = Path.Combine(path, "Messages.db");
            ApplicationShared.instance().initialize(new AndroidUserPreferences(), new SQLitePlatformAndroid(), path);

            BindService(new Intent(this, typeof(SocketService)), this, Bind.AutoCreate);
        }

        public override void OnCreate()
        {
            base.OnCreate();
            // Create your application here
        }


        public SocketClientAdapter getSocketClient()
        {
            return m_socket_client;
        }

        public User getUser()
        {
            return ApplicationShared.instance().getUser();
        }

        public String getPhoneId()
        {
            return "DD";
        }


        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            System.Diagnostics.Debug.WriteLine("OnServiceConnected");
            m_socket_client = (SocketClientAdapter) service;
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            System.Diagnostics.Debug.WriteLine("OnServiceDisconnected");
            m_socket_client = null;
        }
    }
}