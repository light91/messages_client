using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;


namespace messages_pcl.Droid
{
    public class MessagesListAdapter : BaseAdapter<MessageItem>, OnNotifyDataChanged
    {
        private static String TAG_ID = "TAG_ID";

        private Activity m_activity;
        private bool m_edit_mode;

        public MessagesListAdapter(Activity activity) : base()
        {
            m_activity = activity;
            MessageManager.instance().AddNotifyDataChangedListener(this);
        }

        public void OnNotifyDataChanged()
        {
            (m_activity).RunOnUiThread(() =>
            {
                try
                {
                    NotifyDataSetChanged();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("error " + e.GetType() + " " + e.Message);
                }
            });
        }

        public override MessageItem this[int position]
        {
            get
            {
                return MessageManager.instance().get(position);
            }
        }

        public override long GetItemId(int position)
        {
            return position;// MessageManager.instance().getItem(position).getId();
        }

        public override int Count
        {
            get
            {
                return MessageManager.instance().getCount();
            }
        }

        public override View GetView(int position, View view, ViewGroup parent)
        {
            if (view == null)
                view = (ApplicationExt.instance().GetSystemService(Context.LayoutInflaterService) as LayoutInflater).Inflate(Resource.Layout.MessageItemAdapter, parent, false);

            MessageItem item = this[position];

            view.FindViewById<TextView>(Resource.Id.Title).SetText(item.Title, TextView.BufferType.Normal);
            view.FindViewById<TextView>(Resource.Id.Date).SetText(item.Date, TextView.BufferType.Normal);
            view.FindViewById<TextView>(Resource.Id.Desc).SetText(item.Desc, TextView.BufferType.Normal); 
            view.FindViewById<TextView>(Resource.Id.Type).SetText(item.Type, TextView.BufferType.Normal);

            view.FindViewById(Resource.Id.bownlish_line).Visibility = item.Readed ? ViewStates.Gone : ViewStates.Visible;
            view.FindViewById<TextView>(Resource.Id.Title).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Date).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Desc).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            view.FindViewById<TextView>(Resource.Id.Type).SetTypeface(Android.Graphics.Typeface.Default, item.Readed ? Android.Graphics.TypefaceStyle.Normal : Android.Graphics.TypefaceStyle.Bold);
            //view.FindViewById(Resource.Id.message_layout).SetTag(item.getId(), TAG_ID);
            view.FindViewById(Resource.Id.Selected).Visibility = m_edit_mode ? ViewStates.Visible : ViewStates.Gone;

            view.FindViewById<CheckBox>(Resource.Id.Selected).SetOnCheckedChangeListener(new EditModeCheckedListener(item));

            return view;
        }

        public void openEditMode()
        {
            m_edit_mode = true;
            OnNotifyDataChanged();
        }

        public void openViewMode()
        {
            m_edit_mode = false;
            OnNotifyDataChanged();
        }

        public bool isEditMode()
        {
            return m_edit_mode;
        }
    }

    class EditModeCheckedListener : Java.Lang.Object, Android.Widget.CompoundButton.IOnCheckedChangeListener
    {
        private MessageItem m_item;

        public EditModeCheckedListener(MessageItem item)
        {
            m_item = item;
        }

        public void OnCheckedChanged(CompoundButton buttonView, bool isChecked)
        {
            m_item.SetRemovedPrepare(isChecked);
        }
    }
}