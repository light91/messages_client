﻿using System;

using Android.App;
using Android.Text;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Text.Style;

using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V7.Graphics.Drawable;
using Android.Views.InputMethods;
using static Android.Views.View;

namespace messages_pcl.Droid
{
	[Activity (Label = "messages_pcl.Droid")]
	public class MainActivity : ActionBarActivity
    {
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            
			SetContentView (Resource.Layout.Main);

            DrawerLayout mDrawerLayout;
            ActionBarDrawerToggle mDrawerToggle;


            DrawerArrowDrawable m_navigation_icon = new DrawerArrowDrawable(this);
            m_navigation_icon.Color    = Resource.Color.bownish_orange;
            m_navigation_icon.Progress = 1.0f;

            Android.Support.V7.Widget.Toolbar toolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.NavigationIcon = m_navigation_icon;
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            toolbar.Touchables[0].SetOnClickListener(new ArrowClickListener(this));

            /*
            mDrawerLayout = (DrawerLayout) FindViewById(Resource.Id.drawer_layout);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, Resource.String.app_name, Resource.String.app_name);
            mDrawerLayout.SetDrawerListener(mDrawerToggle);*/



            /*
            Android.Support.V7.Widget.Toolbar toolbar = (Android.Support.V7.Widget.Toolbar) FindViewById(Resource.Id.toolbar);
            toolbar.NavigationIcon = m_navigation_icon;
            toolbar.SetTitleTextColor(Android.Graphics.Color.White);
            //setSupportActionBar(toolbar);
            SetActionBar(toolbar);
            //toolbar.getTouchables().get(0).setOnClickListener(m_func_on_click_toolbar);
            //m_func_on_click_hamburger = funcShowMenu;*/

            Fragment fragment = new MainFragment();
            fragmentForward(fragment);
        }
        

        private Fragment getTop()
        {
            Fragment topFragment = FragmentManager.FindFragmentByTag("FFRAGMENT_" + (getBackStackEntryCount() - 1));
            return topFragment;
        }

        public void fragmentClear(Fragment fragment)
        {
            while (getBackStackEntryCount() > 0)
                FragmentManager.PopBackStackImmediate();
            fragmentForward(fragment);
        }

        public int getBackStackEntryCount()
        {
            return FragmentManager.BackStackEntryCount;
        }

        public void fragmentForward(Fragment fragment)
        {
            hideKeyboard();

            Fragment topFragment = getTop();
            if (topFragment != null)
            {
                topFragment.View.Visibility = ViewStates.Gone;
            }
            FragmentTransaction fragmentTransaction = FragmentManager.BeginTransaction();
            fragmentTransaction.Add(Android.Resource.Id.Content, fragment, "FFRAGMENT_" + getBackStackEntryCount());
            fragmentTransaction.SetCustomAnimations(Android.Resource.Animator.FadeIn, Android.Resource.Animator.FadeOut);
            fragmentTransaction.Show(fragment);
            fragmentTransaction.AddToBackStack(null);
            fragmentTransaction.Commit();
            if (topFragment != null)
            {
                topFragment.View.Visibility = ViewStates.Gone;
                topFragment.OnPause();
            }
        }

        public void fragmentBack()
        {
            Fragment topFragment = getTop();
            if (topFragment != null)
                if (topFragment is CheckPrepareFragmentBack)
                if (!(topFragment as CheckPrepareFragmentBack).checkPrepareFragmentBack())
                    return;

            hideKeyboard();
            
            if (getBackStackEntryCount() > 1)
            {
                FragmentManager.PopBackStackImmediate();
                topFragment = getTop();
                if (topFragment != null)
                {
                    topFragment.OnStart();
                    topFragment.View.Visibility = ViewStates.Visible;
                }
                return;
            }
            MoveTaskToBack(true);
        }

        public void activityBack()
        {
            Finish();
        }

        public void onBackPressed()
        {
            fragmentBack();
        }

        private void hideKeyboard()
        {

            View focusView =  CurrentFocus;
            if (focusView != null)
            {
                InputMethodManager imm = (InputMethodManager) GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(focusView.WindowToken, 0);
            }
        }
    };

    public class ArrowClickListener : Java.Lang.Object, IOnClickListener
    {
        private MainActivity m_activity;


        public ArrowClickListener(MainActivity activity)
        {
            m_activity = activity;
        }

        public void OnClick(View v)
        {
            m_activity.fragmentBack();
        }
    }
}


