﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;

using Android.Webkit;

namespace messages_pcl.Droid
{
    [Activity(Label = "MessagesListActivity")]
    public class MessageItemFragment : Fragment
    {
        public static String KEY_MESSAGE_ID = "KEY_MESSAGE_ID";

        private int m_index;
        
        public static MessageItemFragment instance(int index)
        {
            MessageItemFragment fragment = new MessageItemFragment();
            fragment.Arguments = new Bundle();
            fragment.Arguments.PutInt(KEY_MESSAGE_ID, index);
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            m_index = Arguments.GetInt(KEY_MESSAGE_ID, 0);
            MessageItem item = getItem();

            if (!item.Readed)
                item.setReaded();

            View view = inflater.Inflate(Resource.Layout.MessageItemFragment, container, false);
            view.FindViewById<TextView>(Resource.Id.name).SetText(item.Title, TextView.BufferType.Normal);

            view.FindViewById<WebView>(Resource.Id.html).Settings.JavaScriptEnabled = true;
            view.FindViewById<WebView>(Resource.Id.html).Settings.LoadsImagesAutomatically = true;
            view.FindViewById<WebView>(Resource.Id.html).Settings.AllowContentAccess = true;

            view.FindViewById<WebView>(Resource.Id.html).Settings.JavaScriptCanOpenWindowsAutomatically = true;
            view.FindViewById<WebView>(Resource.Id.html).Settings.SetSupportMultipleWindows(true);
            view.FindViewById<WebView>(Resource.Id.html).SetWebViewClient(new WebViewClient());
            view.FindViewById<WebView>(Resource.Id.html).SetWebChromeClient(new WebChromeClient());
            view.FindViewById<WebView>(Resource.Id.html).Settings.AllowContentAccess = true;
            view.FindViewById<WebView>(Resource.Id.html).Settings.DefaultTextEncodingName = "utf-8";
            view.FindViewById<WebView>(Resource.Id.html).LoadData(item.Html, "text/html; charset=utf-8", "utf-8");

            return view;
        }

        private MessageItem getItem()
        {
            return MessageManager.instance().get(m_index);
        }
    }

}