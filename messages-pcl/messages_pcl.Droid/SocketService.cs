using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;

namespace messages_pcl.Droid
{
    [Service]
    public class SocketService : Service
    {
        private SocketClientAdapter m_binder;


        public override IBinder OnBind(Intent intent)
        {
            m_binder = new SocketClientAdapter(this);
            SocketConnect();
            return m_binder;
        }

        public void SocketConnect()
        {
            new Thread(() =>
            {
                while (!m_binder.connect())
                {
                    System.Threading.Tasks.Task.Delay(1000 * 30).Wait();
                }
                m_binder.receiveMessages();
            }).Start();
        }
    }

    public class SocketClientAdapter : Binder, SocketConnectionStaus
    {
        private SocketService m_service;
        private SocketClient m_adaptee;
        private Thread m_thread_socket_receive;
        private SocketConnectionStaus m_socket_connection_status;

        public SocketClientAdapter(SocketService service)
        {
            m_service = service;
            m_adaptee = new SocketClient();
            m_adaptee.setSocketConnectionStatus(this);
        }


        public bool connect()
        {
            return m_adaptee.connect().Result;
        }

        public void disconnect()
        {
            m_adaptee.disconnect();
        }

        public void setSocketConnectionStatus(SocketConnectionStaus socketConnectionStatus)
        {
            m_socket_connection_status = socketConnectionStatus;
        }

        public bool isConnected()
        {
            return m_adaptee.isConnected();
        }

        public void receiveMessages()
        {
            m_thread_socket_receive = new Thread(delegate ()
            {
                m_adaptee.startReceive();
            });
            m_thread_socket_receive.Start();
        }


        public void login(String phone, String code, String password)
        {
            m_adaptee.login(phone, code, password);
        }

        public void getPassword(String phone, String code)
        {
            m_adaptee.getPassword(phone, code);
        }

        void SocketConnectionStaus.onConnect()
        {
            if (m_socket_connection_status != null)
                m_socket_connection_status.onConnect();
        }

        void SocketConnectionStaus.onDisconnect()
        {
            if (m_socket_connection_status != null)
                m_socket_connection_status.onDisconnect();
            m_service.SocketConnect();
        }
    }
}